# Generated by rust2rpm 13
%bcond_without check
%global __cargo_skip_build 0

%global crate mdevctl

Name:           rust-%{crate}
Version:        0.1.0
Release:        1%{?dist}
Summary:        A mediated device management utility for Linux

# Upstream license specification: LGPL-2.1
License:        LGPLv2
URL:            https://crates.io/crates/mdevctl
Source:         %{crates_source}

ExclusiveArch:  %{rust_arches}

BuildRequires:  rust-packaging

%global _description %{expand:
%{summary}.}

%description %{_description}

%package     -n %{crate}
Summary:        %{summary}

%description -n %{crate} %{_description}

%files       -n %{crate}
%license COPYING
%doc README.md
%{_sbindir}/mdevctl
%{_udevrulesdir}/60-mdevctl.rules
%dir %{_sysconfdir}/mdevctl.d
%{_mandir}/man8/mdevctl.8*

%prep
%autosetup -n %{crate}-%{version_no_tilde} -p1
%cargo_prep

%generate_buildrequires
%cargo_generate_buildrequires

%build
%cargo_build

%install
%cargo_install
mkdir -p %{buildroot}%{_sbindir}
mv %{buildroot}%{_bindir}/mdevctl %{buildroot}%{_sbindir}/mdevctl
mkdir -p %{buildroot}%{_sysconfdir}/mdevctl.d
install -Dpm0644 -t %{buildroot}%{_mandir}/man8 mdevctl.8
install -Dpm0644 -t %{buildroot}%{_udevrulesdir} 60-mdevctl.rules

%if %{with check}
%check
%cargo_test
%endif

%changelog
* Tue Apr 27 10:55:11 CDT 2021 Jonathon Jongsma <jjongsma@redhat.com> - 0.1.0-1
- Initial package
