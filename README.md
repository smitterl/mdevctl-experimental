# WARNING: NOT INTENDED FOR GENERAL USE

# mdev-experimental

This is an experimental rewrite of mdevctl in the [rust
language](http:://www.rust-lang.org) in an attempt to improve the performance.
There is no guarantee that this will ever be more than a prototype, but you are
welcome to play with it and contribute.

# Building

To build and run the program, simply run `cargo run $ARGS` in the working
directory. This will download and build all dependencies in addition to the
program itself.
